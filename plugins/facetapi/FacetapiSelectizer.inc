<?php

/**
 * @file
 * Contains FacetapiSelectizer class.
 *
 * This file is mainly taken form FacetapiSelect.
 * See https://www.drupal.org/project/facetapi_select.
 */

/**
 * Widget that renders facets as select boxes.
 */
class FacetapiSelectizer extends FacetapiWidgetLinks {

  private $urlsCache = array();

  /**
   * {@inheritdoc}
   */
  public function execute() {
    static $count = 0;
    $count++;

    $element = &$this->build[$this->facet['field alias']];
    $variables = $this->buildItems($element);
    $variables = $this->appendDefaultOption($variables);
    $variables['settings'] = $this->settings->settings;

    // Add all settings to javascript to be able to configure selectize.js based
    // on settings.
    $this->jsSettings = array_merge($this->jsSettings, $this->settings->settings);

    $element = drupal_get_form("facetapi_selectizer_facet_form_$count", $variables);

    if ($this->isActive()) {
      $this->build['#attributes']['class'][] = self::getFormSelectedClass();
    }

    $element['#attributes'] = array_merge($element['#attributes'], $this->build['#attributes']);
  }

  /**
   * Sets the facetapi select form selected class.
   *
   * @param string $class
   *   The class in which to set the facetapi select form selected class.
   */
  public static function setFormSelectedClass($class) {
    variable_set('facetapi_selectizer_form_selected_class', $class);
  }

  /**
   * Gets the facetapi select form selected class.
   *
   * @return string
   *   A string that represents the class for the facetapi selected form.
   */
  public static function getFormSelectedClass() {
    return variable_get('facetapi_selectizer_form_selected_class', 'selected');
  }

  /**
   * Determine if the Facet is active.
   *
   * @return bool
   *   Returns TRUE if the Facet is active.
   */
  public function isActive() {
    return (boolean) count($this->build['#adapter']->getActiveItems($this->facet->getFacet()));
  }

  /**
   * Build the options array for the provided facetapi element.
   *
   * @param array $element
   *   The facetapi element as provided by execute().
   * @param int $depth
   *   The facetapi depth.
   *
   * @return array
   *   An array that contains the options and active items arrays.
   */
  protected function buildItems(array $element, $depth = 0) {
    $variables = array('options' => array());
    $facet_settings = $this->facet->getSettings();

    $url = NULL;
    foreach ($element as $value => $item) {
      $url = $this->getUrl($item);
      $item['#markup'] = str_repeat('-', $depth) . ' ' . $item['#markup'];

      if ($facet_settings->settings['limit_active_items'] && $item['#active']) {
        $variables['options'][''] = $this->buildSelectOption($item, $facet_settings->settings);
      }
      else {
        $variables['options'][$url] = $this->buildSelectOption($item, $facet_settings->settings);
      }

      if ($item['#active']) {
        $variables['active'][] = $url;
      }
    }

    if (isset($facet_settings->facet)) {
      $variables['id'] = $facet_settings->facet;
    }

    // When there is only one option and it is active with children let's add
    // the children as options.
    if ($item['#active'] && count($variables['options']) === 1 && !empty($item['#item_children'])) {
      $this->appendChildren($variables, $item, $depth);
    }

    return $variables;
  }

  /**
   * Append the Default option to the available options.
   *
   * @param array $variables
   *   The variables array of built items to be modified that will be passed as
   *   to the facet form.
   *
   * @return array
   *   The modified variables array.
   */
  protected function appendDefaultOption(array &$variables) {
    if (!empty($variables['options'])) {

      $variables['default_option_label'] = theme('facetapi_selectizer_select_option', array(
        'facet_text' => $this->getDefaultOptionLabel(),
        'show_count' => FALSE,
      ));

    }

    return $variables;
  }

  /**
   * Retrieve the facet default option label.
   *
   * @return string
   *   The default option label.
   */
  protected function getDefaultOptionLabel($translatable = TRUE) {
    return $this->getOptionLabel('default_option_label', t('Select filter'), $translatable);
  }

  /**
   * Retrieve the facet default option label.
   *
   * @return string
   *   The default option label.
   */
  protected function getDefaultDropdownTitle($translatable = TRUE) {
    return $this->getDropdownTitle('dropdown_title', '', $translatable);
  }

  /**
   * Retrieve the facet option label for the specified option.
   *
   * @param string $label_key
   *   The label key for which to retrieve the option label.
   * @param string $default
   *   The default string to set the option label when it is not set.
   * @param bool $translatable
   *   TRUE if the option label should be translated and FALSE if not.
   *
   * @return string
   *   The specified option label.
   */
  protected function getOptionLabel($label_key, $default, $translatable = TRUE) {
    $option_label = !empty($this->settings->settings[$label_key]) ? $this->settings->settings[$label_key] : $default;

    if ($translatable) {
      $option_label = self::translateLabel($label_key, $option_label, $this->key);
    }

    return $option_label;
  }

  /**
   * Retrieve the facet option label for the specified option.
   *
   * @param string $label_key
   *   The label key for which to retrieve the option label.
   * @param string $default
   *   The default string to set the option label when it is not set.
   * @param bool $translatable
   *   TRUE if the option label should be translated and FALSE if not.
   *
   * @return string
   *   The specified option label.
   */
  protected function getDropdownTitle($label_key, $default, $translatable = TRUE) {
    $dropdown_title = !empty($this->settings->settings[$label_key]) ? $this->settings->settings[$label_key] : $default;

    if ($translatable) {
      $dropdown_title = self::translateLabel($label_key, $dropdown_title, $this->key);
    }

    return $dropdown_title;
  }

  /**
   * Append children to the active items and options for the facet.
   *
   * @param array $variables
   *   The variables array as provided by FacetapiSlectDropdowns::getItems().
   * @param array $item
   *   The facet item to use for retrieving the children.
   * @param int $depth
   *   The current depth of the childen.
   */
  protected function appendChildren(array &$variables, array $item, $depth = 0) {
    $tmpItems = $this->buildItems($item['#item_children'], ++$depth);

    if (!empty($tmpItems['options'])) {
      $variables['options'] += $tmpItems['options'];
    }
  }

  /**
   * Build an indiviudal select option.
   *
   * @param array $item
   *   The facet item to use for to build the select option for.
   * @param array $facet_settings
   *   The facet settings.
   *
   * @return string
   *   The themed individual select option text.
   */
  protected function buildSelectOption(array $item, array $facet_settings) {
    return theme('facetapi_selectizer_select_option', array(
      'facet_text' => $item['#markup'],
      'facet_count' => $item['#count'],
      'show_count' => !empty($this->settings->settings['show_count']),
      'is_active' => isset($item['#active']) ? $item['#active'] : FALSE,
      'facet_settings' => $facet_settings,
    ));
  }

  /**
   * Retrieve the url for the specified facet item.
   *
   * @param array $item
   *   The facet item to use for retrieving the url.
   * @param bool $append_query
   *   Determines if the query should be appended to the return url -
   *   (optional).
   *
   * @return string
   *   The facet url.
   */
  protected function getUrl(array $item, $append_query = TRUE) {
    if (!isset($this->urlsCache[$item['#path']])) {
      $path = !empty($this->settings->settings['submit_page']) ? $this->settings->settings['submit_page'] : $item['#path'];
      $path = strpos($item['#path'], $path) === 0 ? $item['#path'] : $path;
      $this->urlsCache[$item['#path']] = $path;
    }

    $query = ($append_query) ? $item['#query'] : array();
    return url($this->urlsCache[$item['#path']], array('query' => $query));
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(&$form, &$form_state) {
    parent::settingsForm($form, $form_state);

    // Remove non-applicable options.
    $form['widget']['widget_settings']['links'][$this->id]['nofollow'] = array();
    $form['widget']['widget_settings']['links'][$this->id]['show_expanded'] = array();

    $states = array(
      'visible' => array(
        'select[name="widget"]' => array('value' => $this->id),
      ),
    );

    $form['widget']['widget_settings']['links'][$this->id]['default_option_label'] = array(
      '#title' => t('Default option label'),
      '#type' => 'textfield',
      '#default_value' => $this->getDefaultOptionLabel(FALSE),
      '#states' => $states,
    );
    $form['widget']['widget_settings']['links'][$this->id]['dropdown_title'] = array(
      '#title' => t('Dropdown title'),
      '#type' => 'textfield',
      '#default_value' => $this->getDefaultDropdownTitle(FALSE),
      '#states' => $states,
    );
    $form['widget']['widget_settings']['links'][$this->id]['multiple'] = array(
      '#title' => t('Multi select'),
      '#type' => 'checkbox',
      '#default_value' => !empty($this->settings->settings['multiple']),
      '#states' => $states,
    );
    $form['widget']['widget_settings']['links'][$this->id]['show_count'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show facet count'),
      '#default_value' => !empty($this->settings->settings['show_count']),
      '#states' => $states,
    );
    $form['widget']['widget_settings']['links'][$this->id]['add_delete'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show delete button'),
      '#default_value' => !empty($this->settings->settings['add_delete']),
      '#states' => $states,
    );
    $form['widget']['widget_settings']['links'][$this->id]['ajax_selectors'] = array(
      '#title' => t('Ajax area selectors'),
      '#type' => 'textfield',
      '#description' => 'Use comma separated value. E.g. \'#easy-breadcrumb-easy-breadcrumb\', \'.column.sidebar.first\', \'.content.column\'. If this value is present ajax will be used.',
      '#default_value' => !empty($this->settings->settings['ajax_selectors']) ? $this->settings->settings['ajax_selectors'] : '',
      '#states' => $states,
    );

    $form['#submit'] = empty($form['#submit']) ? array() : $form['#submit'];
    $form['#submit'][] = 'facetapi_selectizer_facet_settings_form_submit';
  }

  /**
   * Helper function to integrate with the i18n_string_translate function.
   *
   * @param string $name
   *   Translation context.
   * @param string $string
   *   The raw db value for the given property.
   * @param string $key
   *   Select key value.
   *
   * @return string
   *   If i18n_string is available the translated string is returned, otherwise,
   *   the supplied string is returned.
   */
  public static function translateLabel($name, $string, $key) {
    if (!function_exists('i18n_string_translate')) {
      return $string;
    }

    return i18n_string_translate(self::getI18nName($name, $key), $string);
  }

  /**
   * Helper function to integrate with the i18n_string_update function.
   *
   * @param string $name
   *   Translation context.
   * @param string $string
   *   The raw db value for the given property.
   * @param string $key
   *   Select key value.
   */
  public static function updateLabelTranslation($name, $string, $key) {
    if (function_exists('i18n_string_update')) {
      i18n_string_update(self::getI18nName($name, $key), $string);
    }
  }

  /**
   * Helper function to integrate with facetapi_selectizer_i18n_string_list.
   *
   * Appends options to an array to send to i18n for translation.
   *
   * @param StdClass $settings
   *   The settings from the active facets.
   * @param array $strings
   *   The referenced array of strings to return to i18n for translation.
   */
  public static function appendTranslatableOption(StdClass $settings, array &$strings = array()) {
    if (!empty($settings->settings['widget']) && $settings->settings['widget'] == 'facetapi_selectizer_dropdowns') {
      $field_name = self::getI18nStringKey($settings->facet);
      $strings['facetapi_selectizer']['default_option_label'][$field_name]['label'] = $settings->settings['default_option_label'];
    }
  }

  /**
   * Retrieve the i18n string key.
   *
   * @param string $key
   *   The facetapi field name for which to retrieve the i18n string key.
   *
   * @return string
   *   The i18n string key to be used with building the i18n name.
   */
  public static function getI18nStringKey($key) {
    return str_replace(':', '_', $key);
  }

  /**
   * Retrieve the i18n name.
   *
   * @param string $name
   *   Translation context.
   * @param string $key
   *   The facetapi field name for which to retrieve the i18n string key.
   *
   * @return array
   *   An array that contains textgroup and string context for i18n.
   */
  public static function getI18nName($name, $key) {
    return array(
      'facetapi_selectizer',
      $name,
      self::getI18nStringKey($key),
      'label',
    );
  }

}
