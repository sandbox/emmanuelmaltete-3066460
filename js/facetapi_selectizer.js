/**
 * @file
 * Handles Facet API Select (ajax) functionality.
 */
(function ($, Drupal) {
  var db = Drupal.behaviors;

  db.facetapiSelectizer = {
    ajaxAreas: ['body'],
    attach: function (context, settings) {
      db.facetapiSelectizer.initSelectize(context, settings);
    },

    ajax: function (url, areas, context, settings) {
      areas = (areas) ? areas.split(',') : [];
      // Initiate filtering by fading out ajax_selector areas.
      if (areas && areas.length > 0) {
        for (let part in areas) {
          var area = areas[part].trim();
          $(area).fadeTo('fast', .4);
        }
      }

      $.ajax({
        url: url
      }).done(function (data) {
        if (areas && areas.length > 0) {
          // Refresh individual areas defined in db.ajaxAreas.
          for (let part in areas) {
            var area = areas[part].trim();
            $(area).replaceWith(
                $(area, data)
            ).fadeTo('fast', 1);
          }
        }

        // Reinitialize selectize.js.
        db.facetapiSelectizer.initSelectize(context, settings);
      })
    },

    initSelectize: function (context, settings) {
      // Default options.
      var options = {
        delimiter: ',',
        persist: false,
      };

      // Loop through facets and initiate selectize.js where applicable.
      if (settings.facetapi.facets.length > 0) {
        for (let facet in settings.facetapi.facets) {
          var facetOptions = settings.facetapi.facets[facet];

          if (facetOptions.widget == 'facetapi_selectizer_dropdowns') {
            var extendedOptions = options;
            var optionsPlugins = {};

            // Set maximum of filters.
            if (!isNaN(facetOptions.limit) && facetOptions.limit > 0) {
              extendedOptions = $.extend({ maxItems: facetOptions.limit }, extendedOptions);
            }

            // Check if ajax selectors are set, then use ajax. Else go to URL on filtering.
            if (facetOptions.ajax_selectors) {
              var ajaxSelectors = facetOptions.ajax_selectors;

              extendedOptions = $.extend({
                onItemAdd: function (value, $item) {
                  db.facetapiSelectizer.ajax(value, ajaxSelectors, context, settings);
                },
                onItemRemove: function (value) {
                  db.facetapiSelectizer.ajax(value, ajaxSelectors, context, settings);
                },
              }, extendedOptions);
            }
            else {
              extendedOptions = $.extend({
                onItemAdd: function (value, $item) {
                  top.location.href = value;
                },
                onItemRemove: function (value) {
                  top.location.href = value;
                },
              }, extendedOptions);
            }

            // Add delete button plugin so each filter item has a delete button.
            if (facetOptions.add_delete) {
              optionsPlugins = $.extend({ 'remove_button': true }, optionsPlugins);

            }

            // Add title for dropdown.
            if (facetOptions.dropdown_title) {
              optionsPlugins = $.extend({ 'dropdown_header': { title: facetOptions.dropdown_title } }, optionsPlugins);
            }

            extendedOptions = $.extend({ plugins: optionsPlugins }, extendedOptions);
          }

          $('.facetapi-select-submit', '#' + facetOptions.id).hide();
          $('.facetapi-select', '#' + facetOptions.id).selectize(extendedOptions);
        }
      }
    }
  };
})(jQuery, Drupal);
